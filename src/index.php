<?php
 // 출력 버퍼를 호출하고,
 ob_start();

 // 브라우저로 출력하고, 출력물을 비웁니다.
 ob_flush();
 flush();

 // 실제적인 출력물을 비워 줍니다.
 for($i=0;$i<1000;$i++){
      echo str_repeat("\n", 4096);
      echo getHostByName(getHostName()).'<br />';
      ob_flush();
      flush();
      ob_clean();
      sleep(3);
 }

 // 출력 버퍼를 종료합니다.
 ob_end_clean();
