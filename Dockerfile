FROM php:7.4-fpm
RUN apt-get update 
RUN apt-get install -y libmagickwand-dev
RUN docker-php-ext-install gd
